package rpccommon

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
)

var ErrInternalServer = &Error{
	Message:    "Internal error",
	Code:       "INTERBAL_SERVER_ERROR",
	HttpStatus: 500,
}

var ErrAuth = &Error{
	Message:    "Auth error",
	Code:       "AUTH_FAIL",
	HttpStatus: 401,
}

type ErrorCode string

type Error struct {
	Message    string          `json:"message"`
	Code       ErrorCode       `json:"message_code"`
	Info       json.RawMessage `json:"info,omitempty"`
	HttpStatus int             `json:"-"`
}

func (e *Error) Error() string {
	if e == nil || e.Code == "" {
		return ""
	}
	if len(e.Info) != 0 {
		return fmt.Sprintf("[%s] %s (%s)", e.Code, e.Message, e.Info)
	}

	return fmt.Sprintf("[%s] %s", e.Code, e.Message)

}

func (e *Error) Is(err error) bool {
	if e == nil || err == nil {
		return false
	}
	var asserted *Error
	if ok := errors.As(err, &asserted); !ok {
		return false
	}

	return e.Code != "" && e.Code == asserted.Code
}

func (e *Error) HTTPStatus() int {
	if e == nil {
		return http.StatusOK
	}
	if e.HttpStatus < 100 || e.HttpStatus >= 600 {
		return http.StatusInternalServerError
	}
	return e.HttpStatus
}

func (e *Error) SetInfo(v any) *Error {
	res := &Error{
		Message:    e.Message,
		Code:       e.Code,
		HttpStatus: e.HttpStatus,
	}

	info, err := json.MarshalIndent(v, "", "  ")

	// Skiping error :(
	if err == nil {
		res.Info = info
	}

	return res
}
