package rpccommon

import (
	"context"
	"encoding/json"

	"github.com/myfantasy/mfctx/jsonify"
)

type Meta map[string]json.RawMessage

func (m Meta) Append(k string, v any) Meta {
	b, err := json.MarshalIndent(v, "", "  ")
	if err != nil {
		return m
	}

	m[k] = b
	return m
}

type Request[T any] struct {
	Meta Meta `json:"meta,omitempty"`
	Data T    `json:"data,omitempty"`

	Params map[string]string `json:"-"`
}

type Responce[T any] struct {
	Meta   Meta     `json:"meta,omitempty"`
	Data   T        `json:"data,omitempty"`
	Error  *Error   `json:"error,omitempty"`
	Errors []*Error `json:"errors,omitempty"`
}

type AuthInfo struct {
	AppName  string `json:"an,omitempty"`
	UserName string `json:"un,omitempty"`
	AuthType string `json:"at,omitempty"`

	Permissions map[string]bool `json:"-"`
}

type RawInfo struct {
	RawRequest []byte
	RawHeaders map[string][]string
}

func (ri RawInfo) String() string {
	return jsonify.JsonifySLn(map[string]any{"RawHeaders": ri.RawHeaders, "RawRequest": string(ri.RawRequest)})
}

type UseCase[TRq, TRs any] func(ctx context.Context, request Request[TRq], ai AuthInfo) (responce TRs, err error)
type UseCaseWideRaw[TRq, TRs any] func(ctx context.Context, request Request[TRq], ai AuthInfo, ri RawInfo) (responce TRs, err error)
