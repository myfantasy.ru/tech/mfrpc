package rpchttpserver

import (
	"encoding/json"
	"io"
	"net/http"

	"github.com/myfantasy/mfctx"
	"gitlab.com/myfantasy.ru/tech/mfrpc/rpccommon"
	"gitlab.com/myfantasy.ru/tools/oncfg"
)

const EmptyAuthType = "none"

const HeaderFeelTag = "header"

const AuthorizationHeader = "Authorization"

var StopHeaders = map[string]bool{
	AuthorizationHeader: true,
}

type RequestGrabber[TRq any] func(ctx *mfctx.Crumps, rq *http.Request) (request rpccommon.Request[TRq], ri rpccommon.RawInfo, err error)
type RequestAuth func(ctx *mfctx.Crumps, rq *http.Request, ri rpccommon.RawInfo) (ai rpccommon.AuthInfo, ok bool, err error)

func DoResponse[TRq, TRs any](segment string, method string, rg RequestGrabber[TRq], ra RequestAuth, uc rpccommon.UseCase[TRq, TRs]) http.HandlerFunc {
	return func(rw http.ResponseWriter, rq *http.Request) {
		ctxIn := rq.Context()
		ctx := mfctx.FromCtx(ctxIn).StartSegment(segment, method)
		var errEnd error
		defer func() { ctx.Complete(errEnd) }()

		if rg == nil {
			rg = RequestGrabberJsonWithRawInfo[TRq]
		}

		request, ri, err := rg(ctx, rq)
		if err != nil {
			errEnd = WriteError(ctx, rw, err)
			return
		}

		if ra == nil {
			ra = RequestAuthEmpty
		}

		ai, ok, err := ra(ctx, rq, ri)
		if err != nil {
			errEnd = WriteError(ctx, rw, err)
			return
		}
		if !ok {
			errEnd = WriteError(ctx, rw, rpccommon.ErrAuth)
			return
		}

		responce, err := uc(ctx, request, ai)
		if err != nil {
			errEnd = WriteError(ctx, rw, err)
			return
		}

		errEnd = WriteResult(ctx, rw, responce)
	}
}

func RequestGrabberJsonWithRawInfo[TRq any](ctx *mfctx.Crumps, rq *http.Request) (request rpccommon.Request[TRq], ri rpccommon.RawInfo, err error) {
	ctx = ctx.Start("rpchttpserver.RequestGrabberJsonWithRawInfo")
	defer func() { ctx.Complete(err) }()

	reader := rq.Body
	defer reader.Close()

	body, err := io.ReadAll(reader)
	if err != nil {
		return request, ri, err
	}

	ri.RawRequest = body

	oncfg.SetDefault(&request)

	err = json.Unmarshal(body, &request)
	if err != nil {
		return request, ri, err
	}

	ri.RawHeaders = map[string][]string{}
	rawHeadersString := map[string]string{}

	for k, v := range rq.Header {
		if StopHeaders[k] {
			continue
		}

		ri.RawHeaders[k] = v
		if len(v) > 0 {
			rawHeadersString[k] = v[0]
		}
	}

	request.Params = rawHeadersString

	oncfg.FeelConfigWide(&request.Data, rawHeadersString, "", HeaderFeelTag, "", false, false)

	return request, ri, nil
}

func RequestAuthEmpty(ctx *mfctx.Crumps, rq *http.Request, ri rpccommon.RawInfo) (ai rpccommon.AuthInfo, ok bool, err error) {
	ctx = ctx.Start("rpchttpserver.RequestAuthEmpty")
	defer func() { ctx.Complete(err) }()

	return rpccommon.AuthInfo{
		AppName:  "",
		UserName: "",
		AuthType: EmptyAuthType,

		Permissions: make(map[string]bool),
	}, true, nil
}
