package rpchttpserver

import (
	"encoding/json"
	"net/http"
	"time"

	"github.com/myfantasy/mfctx"
	"gitlab.com/myfantasy.ru/tech/mfrpc/rpccommon"
	"gitlab.com/myfantasy.ru/tools/oncfg"
)

const CountOutBytesParam = "out_bytes"
const StatusCodeParam = "status_code"
const AppIDMeta = "app_id"
const AppNameMeta = "app_name"
const AppVersionMeta = "app_ver"
const OperationIDMeta = "op_id"
const OperationDate = "dt"

func WriteError(ctx *mfctx.Crumps, rw http.ResponseWriter, err error) (errInt error) {
	ctx = ctx.Start("rpchttpserver.WriteResult")
	defer func() { ctx.Complete(errInt) }()

	if err == nil {
		err = rpccommon.ErrInternalServer
	}

	errE, ok := err.(*rpccommon.Error)
	if !ok {
		errE = rpccommon.ErrInternalServer
	}

	meta := rpccommon.Meta{}.
		Append(AppIDMeta, mfctx.GetAppID()).
		Append(AppNameMeta, mfctx.GetAppName()).
		Append(AppVersionMeta, mfctx.GetAppVersion()).
		Append(OperationIDMeta, ctx.GetOperationID()).
		Append(OperationDate, time.Now())

	b, errInt := json.MarshalIndent(
		rpccommon.Responce[*struct{}]{
			Meta:  meta,
			Error: errE,
		}, "", "  ",
	)
	if errInt != nil {
		return errInt
	}

	cnt, errInt := rw.Write(b)
	ctx.With(CountOutBytesParam, cnt)

	rw.WriteHeader(errE.HTTPStatus())
	ctx.With(StatusCodeParam, errE.HTTPStatus())

	return errInt
}

func WriteResult[TRs any](ctx *mfctx.Crumps, rw http.ResponseWriter, responce TRs) (errInt error) {
	ctx = ctx.Start("rpchttpserver.WriteResult")
	defer func() { ctx.Complete(errInt) }()

	meta := rpccommon.Meta{}.
		Append(AppIDMeta, mfctx.GetAppID()).
		Append(AppNameMeta, mfctx.GetAppName()).
		Append(AppVersionMeta, mfctx.GetAppVersion()).
		Append(OperationIDMeta, ctx.GetOperationID()).
		Append(OperationDate, time.Now())

	b, errInt := json.MarshalIndent(
		rpccommon.Responce[TRs]{
			Meta: meta,
			Data: responce,
		}, "", "  ",
	)
	if errInt != nil {
		return errInt
	}

	headers := make(map[string]string)
	oncfg.GrabConfigWide(&responce, headers, "", HeaderFeelTag, "")

	for k, v := range headers {
		rw.Header().Add(k, v)
	}

	cnt, errInt := rw.Write(b)
	ctx.With(CountOutBytesParam, cnt)

	rw.WriteHeader(http.StatusOK)

	return errInt
}
