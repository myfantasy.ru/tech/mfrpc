package rpchttpclient

import (
	"crypto/tls"
	"net/http"
	"time"
)

type ClientConfig struct {
	UriPrefix          string        `envconfig:"uri_prefix" default:"http://localhost:3000" json:"uri_prefix"`
	Destination        string        `envconfig:"dest" default:"" json:"dest"`
	Timeout            time.Duration `envconfig:"timeout" default:"10s" json:"timeout"`
	InsecureSkipVerify bool          `envconfig:"insecure_skip_verify" default:"false" json:"insecure_skip_verify"`
}

func (cfg *ClientConfig) CreateClient() (*Client, error) {
	client := &http.Client{}
	return &Client{
		UriPrefix:   func() string { return cfg.UriPrefix },
		Destination: func() string { return cfg.Destination },
		Client:      func() *http.Client { return cfg.HttpClientRefeel(client) },

		cfg: cfg,
	}, nil
}

func (cfg *ClientConfig) HttpClientRefeel(client *http.Client) *http.Client {
	if client.Timeout != cfg.Timeout {
		client.Timeout = cfg.Timeout
	}

	if client.Transport == nil {
		client.Transport = &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: false},
		}
	}

	tr, ok := client.Transport.(*http.Transport)
	if ok {
		if tr.TLSClientConfig.InsecureSkipVerify != cfg.InsecureSkipVerify {
			tr.TLSClientConfig.InsecureSkipVerify = cfg.InsecureSkipVerify
		}
	}

	return client
}
