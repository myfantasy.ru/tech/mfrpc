package rpchttpclient

import (
	"context"
	"fmt"
	"sync"

	"github.com/myfantasy/mfctx"
	"github.com/myfantasy/poh"
	"gitlab.com/myfantasy.ru/tools/oncfg"
)

var ErrHttpClientHubClosePoint = fmt.Errorf("close point in hub")

type HttpClientHub struct {
	ctxBase  context.Context
	ctxClose context.CancelCauseFunc

	cfg *oncfg.ListSourceConfig
	hub *poh.Hub[string, *Client]

	mx sync.Mutex
}

func MakeHttpClientHub(ctxBase context.Context) *HttpClientHub {
	res := &HttpClientHub{
		ctxBase: ctxBase,
		cfg:     oncfg.ListSourceConfigCreate(),
	}

	res.hub = poh.MakeHub[string, *Client](
		ctxBase,
		func(ctx context.Context) (keys []string, err error) {
			return res.cfg.Keys(ctx)
		},
		func(ctx context.Context, key string, point *Client) (err error) {
			return nil
		},
		func(ctxIn context.Context, key string) (point *Client, err error) {
			if res.ctxBase.Err() != nil {
				return nil, res.ctxBase.Err()
			}

			cfg := &ClientConfig{}

			oncfg.SetDefault(cfg)
			oncfg.SetDefaultPwd(cfg)

			ctx := mfctx.FromCtx(ctxIn).Copy().With("key", key)

			rCfg, err := res.cfg.Config(ctx, key)
			if err != nil {
				return nil, err
			}

			err = cfg.FeelCfg(ctx, &rCfg)
			if err != nil {
				return nil, err
			}

			mpl, err := cfg.CreateClient()
			if err != nil {
				return nil, err
			}

			return mpl, nil
		},
		func(ctxIn context.Context, key string, point *Client) (err error) {
			cfg := point.Cfg()

			ctx := mfctx.FromCtx(ctxIn).Copy().With("key", key)

			rCfg, err := res.cfg.Config(ctx, key)
			if err != nil {
				return err
			}

			err = cfg.FeelCfg(ctx, &rCfg)
			if err != nil {
				return err
			}

			return nil
		},
	)

	res.cfg.SetCallAfterUpdateFunc(func(ctx context.Context) {
		res.hub.Refresh()
	})

	return res
}

func (hchub *HttpClientHub) Cfg() *oncfg.ListSourceConfig {
	hchub.mx.Lock()
	defer hchub.mx.Unlock()

	return hchub.cfg
}

func (hchub *HttpClientHub) Get(key string) (point *Client, ok bool) {
	hchub.mx.Lock()
	defer hchub.mx.Unlock()

	return hchub.hub.Get(key)
}

// Close - calls ctxClose if its set (error should be set)
func (hchub *HttpClientHub) Close(err error) {
	hchub.mx.Lock()
	defer hchub.mx.Unlock()

	if hchub.ctxClose != nil {
		hchub.ctxClose(err)
	}
}
