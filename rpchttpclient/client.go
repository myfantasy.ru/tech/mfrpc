package rpchttpclient

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"net/http"

	"github.com/myfantasy/mfctx"
	"github.com/myfantasy/mfctx/jsonify"
	"gitlab.com/myfantasy.ru/tech/mfrpc/rpccommon"
	"gitlab.com/myfantasy.ru/tools/oncfg"
)

const PathLogParam = "path"
const UriPrefixLogParam = "uri_prefix"
const DestinationLogParam = "dst"
const SendBytesParam = "out_bytes"
const GetBytesParam = "in_bytes"
const GetStatusCode = "get_status_code"

const AppIDMeta = "app_id"
const AppNameMeta = "app_name"
const AppVersionMeta = "app_ver"
const OperationIDMeta = "op_id"

const HeaderFeelTag = "header"
const AuthorizationHeader = "Authorization"

var StopHeaders = map[string]bool{
	AuthorizationHeader: true,
}

type GenerateRequestFunc[TRq any] func(ctx *mfctx.Crumps, c *Client, path string, rq TRq) (*http.Request, error)
type GenerateAuthFunc[TRq any] func(ctx *mfctx.Crumps, c *Client, path string, req *http.Request, rq TRq) error
type ParseResponseFunc[TRs any] func(ctx *mfctx.Crumps, c *Client, resp *http.Response) (rs *rpccommon.Responce[TRs], rr *RawRespone, err error)

type UriPrefixFunc func() string
type DestinationFunc func() string
type HttpClientFunc func() *http.Client

type Client struct {
	// UriPrefix - first path of uri like `https://www.example.com:8080/api`
	UriPrefix UriPrefixFunc
	// Destination - text destination info like `My Best Remote Service`
	Destination DestinationFunc

	// Client - http client for send request
	Client HttpClientFunc

	cfg *ClientConfig
}

func (c *Client) Cfg() *ClientConfig {
	return c.cfg
}

type Request[TRq, TRs any] struct {

	// Path for call like `/v5/orders`
	Path string

	GenerateRequest GenerateRequestFunc[TRq]
	GenerateAuth    GenerateAuthFunc[TRq]
	ParseResponse   ParseResponseFunc[TRs]
}

type RawRespone struct {
	StatusCode int
	Body       []byte
	Headers    map[string]string
}

func (rr RawRespone) String() string {
	return jsonify.JsonifySLn(map[string]any{"StatusCode": rr.StatusCode, "Body": string(rr.Body), "Headers": rr.Headers})
}

func (r Request[TRq, TRs]) Call(ctxIn context.Context, client *Client, rq TRq) (rs *rpccommon.Responce[TRs], rr *RawRespone, err error) {
	ctx := mfctx.FromCtx(ctxIn).Start("rpchttpclient.Request.Call")
	ctx.With(PathLogParam, r.Path)
	ctx.With(UriPrefixLogParam, client.UriPrefix())
	ctx.With(DestinationLogParam, client.Destination())
	defer func() { ctx.Complete(err) }()

	if r.GenerateRequest == nil {
		r.GenerateRequest = GenerateRequestJson
	}
	if r.GenerateAuth == nil {
		r.GenerateAuth = GenerateAuthEmpty
	}
	if r.ParseResponse == nil {
		r.ParseResponse = ParseResponseJson
	}

	req, err := r.GenerateRequest(ctx, client, r.Path, rq)
	if err != nil {
		return nil, nil, err
	}

	err = r.GenerateAuth(ctx, client, r.Path, req, rq)
	if err != nil {
		return nil, nil, err
	}

	resp, err := client.Client().Do(req)
	if err != nil {
		return nil, nil, err
	}
	defer resp.Body.Close()

	rs, rr, err = r.ParseResponse(ctx, client, resp)

	return rs, rr, err
}

func GenerateRequestJson[TRq any](ctx *mfctx.Crumps, c *Client, path string, rq TRq) (*http.Request, error) {
	meta := rpccommon.Meta{}.
		Append(AppIDMeta, mfctx.GetAppID()).
		Append(AppNameMeta, mfctx.GetAppName()).
		Append(AppVersionMeta, mfctx.GetAppVersion()).
		Append(OperationIDMeta, ctx.GetOperationID())

	b, err := json.Marshal(rpccommon.Request[any]{
		Meta: meta,
		Data: rq,
	})
	if err != nil {
		return nil, err
	}

	ctx.With(SendBytesParam, len(b))

	bdy := bytes.NewReader(b)
	req, err := http.NewRequestWithContext(ctx, http.MethodPost, c.UriPrefix()+path, bdy)
	if err != nil {
		return nil, err
	}

	headers := make(map[string]string)
	oncfg.GrabConfigWide(&rq, headers, "", HeaderFeelTag, "")

	for k, v := range headers {
		req.Header.Add(k, v)
	}

	return req, nil
}

func GenerateAuthEmpty[TRq any](ctx *mfctx.Crumps, c *Client, path string, req *http.Request, rq TRq) error {
	return nil
}

func ParseResponseJson[TRs any](ctx *mfctx.Crumps, c *Client, resp *http.Response) (rs *rpccommon.Responce[TRs], rr *RawRespone, err error) {
	rr = &RawRespone{
		StatusCode: resp.StatusCode,
	}

	ctx.With(GetStatusCode, resp.StatusCode)

	rBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, rr, err
	}
	rr.Body = rBody

	ctx.With(GetBytesParam, len(rBody))

	rs = &rpccommon.Responce[TRs]{}

	err = json.Unmarshal(rBody, rs)
	if err != nil {
		return nil, rr, err
	}

	rawHeadersString := map[string]string{}

	for k, v := range resp.Header {
		if StopHeaders[k] {
			continue
		}
		if len(v) > 0 {
			rawHeadersString[k] = v[0]
		}
	}

	oncfg.FeelConfigWide(&rs.Data, rawHeadersString, "", HeaderFeelTag, "", false, false)

	rr.Headers = rawHeadersString

	return rs, rr, nil
}
