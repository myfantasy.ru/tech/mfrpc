package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/myfantasy/mfctx/jsonify"
	"gitlab.com/myfantasy.ru/tech/mfrpc/rpccommon"
	"gitlab.com/myfantasy.ru/tech/mfrpc/rpchttpclient"
	"gitlab.com/myfantasy.ru/tech/mfrpc/rpchttpserver"
	"gitlab.com/myfantasy.ru/tools/oncfg"
)

type RequestExample struct {
	A int
	B string
}

type ResponseExample struct {
	A int
	B string
}

func main() {
	fmt.Println("GO GO GO")
	// Server

	helloHandler := rpchttpserver.DoResponse[RequestExample, ResponseExample](
		"segment_name", "method_name", nil, nil,
		func(ctx context.Context, request rpccommon.Request[RequestExample], ai rpccommon.AuthInfo) (responce ResponseExample, err error) {
			return ResponseExample{
				A: 10 + request.Data.A,
				B: "WOW_" + request.Data.B,
			}, nil
		},
	)

	mux := http.NewServeMux()
	mux.HandleFunc("/hello", helloHandler)

	go func() {
		err := http.ListenAndServe(":3003", mux)
		log.Fatal(err)
	}()

	go func() {
		err := http.ListenAndServe(":3001", mux)
		log.Fatal(err)
	}()

	go func() {
		err := http.ListenAndServe(":3002", mux)
		log.Fatal(err)
	}()

	time.Sleep(100 * time.Millisecond)

	// Client
	ctxBase := context.Background()
	cHub := rpchttpclient.MakeHttpClientHub(ctxBase)

	err := cHub.Cfg().FeelCfg(ctxBase,
		&oncfg.ConfigRaw{
			Cfg: []byte(`{
	"client1": {
		"uri_prefix": "http://localhost:3001"
	},
	"client2": {
		"uri_prefix": "http://localhost:3002"
	},
	"client3": {
		"uri_prefix": "http://localhost:3003"
	}
}`),
		},
	)
	if err != nil {
		log.Fatal(err)
	}

	// wait for load
	fmt.Println("update start. Let`s sleep 2 sec")
	time.Sleep(2 * time.Second)

	client, ok := cHub.Get("client1")

	if !ok {
		log.Fatal("should be OK")
	}

	r := rpchttpclient.Request[RequestExample, ResponseExample]{
		Path: "/hello",
	}

	rs, rr, err := r.Call(context.Background(), client, RequestExample{5, "Hi"})

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(rr)

	fmt.Println(jsonify.JsonifySLn(rs))
}
