package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/myfantasy/mfctx/jsonify"
	"gitlab.com/myfantasy.ru/tech/mfrpc/rpccommon"
	"gitlab.com/myfantasy.ru/tech/mfrpc/rpchttpclient"
	"gitlab.com/myfantasy.ru/tech/mfrpc/rpchttpserver"
	"gitlab.com/myfantasy.ru/tools/oncfg"
)

type RequestExample struct {
	A int
	B string

	C string `header:"X-Test-Head" json:"-"`
}

type ResponseExample struct {
	A int
	B string

	C string `header:"X-Test-Head" json:"-"`
}

func main() {
	fmt.Println("GO GO GO")
	// Server

	helloHandler := rpchttpserver.DoResponse[RequestExample, ResponseExample](
		"segment_name", "method_name", nil, nil,
		func(ctx context.Context, request rpccommon.Request[RequestExample], ai rpccommon.AuthInfo) (responce ResponseExample, err error) {

			return ResponseExample{
				A: 10 + request.Data.A,
				B: "WOW_" + request.Data.B,
				C: "WOW_" + request.Data.C,
			}, nil
		},
	)

	mux := http.NewServeMux()
	mux.HandleFunc("/hello", helloHandler)

	go func() {
		err := http.ListenAndServe(":3000", mux)
		log.Fatal(err)
	}()

	time.Sleep(100 * time.Millisecond)

	// Client
	ccfg := &rpchttpclient.ClientConfig{}

	oncfg.SetDefault(ccfg)
	oncfg.SetDefaultPwd(ccfg)

	ccfg.Timeout = 5 * time.Second

	client, err := ccfg.CreateClient()

	if err != nil {
		log.Fatal(err)
	}

	r := rpchttpclient.Request[RequestExample, ResponseExample]{
		Path: "/hello",
	}

	rs, rr, err := r.Call(context.Background(), client, RequestExample{5, "Hi", "head"})

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("!!!!!!")
	fmt.Println(rr)
	fmt.Println("----!!!!!!")
	fmt.Println(jsonify.JsonifySLn(rs))
}
