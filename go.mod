module gitlab.com/myfantasy.ru/tech/mfrpc

go 1.21.4

require (
	github.com/myfantasy/mfctx v1.0.1
	github.com/myfantasy/poh v1.0.1
	gitlab.com/myfantasy.ru/tools/oncfg v1.0.1
)

require (
	github.com/cespare/xxhash v1.1.0 // indirect
	github.com/go-logr/logr v1.4.1 // indirect
	github.com/go-logr/stdr v1.2.2 // indirect
	github.com/myfantasy/ints v1.0.1 // indirect
	go.opentelemetry.io/otel v1.25.0 // indirect
	go.opentelemetry.io/otel/metric v1.25.0 // indirect
	go.opentelemetry.io/otel/trace v1.25.0 // indirect
	golang.org/x/exp v0.0.0-20240416160154-fe59bbe5cc7f // indirect
)
